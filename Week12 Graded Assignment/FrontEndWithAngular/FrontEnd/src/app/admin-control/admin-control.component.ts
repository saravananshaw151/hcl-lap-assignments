import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-admin-control',
  templateUrl: './admin-control.component.html',
  styleUrls: ['./admin-control.component.css']
})
export class AdminControlComponent implements OnInit {
  
  
  Logres:string="";
   Regres:string="";
   LoginRef = new FormGroup({
    email:new FormControl("",[Validators.required,Validators.pattern("\[a-z]+[0-9]+@[a-z]+.com")]),
    password:new FormControl("",[Validators.required,Validators.minLength(6)])
  });
  RegRef = new FormGroup({
    email: new FormControl("",[Validators.required,Validators.pattern("\[a-z]+[0-9]+@[a-z]+.com")]),
    password: new FormControl("",[Validators.required,Validators.minLength(6)]),
    username: new FormControl("",[Validators.required,Validators.minLength(7)])
  });

  constructor(public adser:AdminService,public route:Router) { }

  ngOnInit(): void {
  }
  
  adminLogin():void{
    let login = this.LoginRef.value;
     this.adser.adminSignIn(login).subscribe(res=>{
      if(res.startsWith("Welcome")){
        sessionStorage.setItem("adname",login.email);
        this.route.navigate(["adminhome"]);
      }
      else{
        this.Logres=res;
        this.LoginRef.reset();}
    },err=>console.log(err),()=>console.log("User Login"));
   
     
  }

  adminRegister():void{
    let reg = this.RegRef.value;
    this.adser.adminSignUp(reg).subscribe(res=>this.Regres=res,err=>console.log(err),()=>console.log("User Register"));
    this.RegRef.reset();
  }

}

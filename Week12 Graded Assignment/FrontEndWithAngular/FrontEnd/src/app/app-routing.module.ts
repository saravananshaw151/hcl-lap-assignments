import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminControlComponent } from './admin-control/admin-control.component';
import { AdminhomepageComponent } from './adminhomepage/adminhomepage.component';
import { UsercontrolComponent } from './usercontrol/usercontrol.component';
import { UserhomepageComponent } from './userhomepage/userhomepage.component';

const routes: Routes = [
  {path:"adminlogin", component:AdminControlComponent},
  {path:"userlogin",component:UsercontrolComponent},
  {path:"userhome",component:UserhomepageComponent},
  {path:"adminhome",component:AdminhomepageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

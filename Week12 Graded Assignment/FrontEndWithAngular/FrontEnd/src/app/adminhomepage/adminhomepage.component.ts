import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { BookEntity } from '../book-entity';
import { BookService } from '../book.service';
import { UserEntity } from '../user-entity';
import { UserService } from '../user.service';

@Component({
  selector: 'app-adminhomepage',
  templateUrl: './adminhomepage.component.html',
  styleUrls: ['./adminhomepage.component.css']
})
export class AdminhomepageComponent implements OnInit {
   
   admin:string="";

   retrieveUsers:Array<UserEntity>=[];
  getbooks:Array<BookEntity>=[];
  storeUs:string="";
  storeMsg:string="";
  deleteMsg:string="";
  updateMsg:string="";
  deleteUs:string="";
  updateUs:string="";
  flag:boolean=false;
  id:number=0;
  userfullname:string="";
  contact:number=0;
  gender:string="";
  address:string="";
  city:string="";
  state:string="";
  fl:boolean=false;
  bid:number=0;
  price:number=0;
  bookname:string="";
  author:string="";
  bookgenre:string="";
  bookimage:string="";

   
  constructor(public route:Router,public UsServ:UserService,public bookServ:BookService) { }

  ngOnInit(): void {
    let obj = sessionStorage.getItem("adname");
    if(obj!=null){
      this.admin=obj;
    }
    this.getUsers();
    this.getBooks();
  }

  logout() {
    sessionStorage.removeItem("adname");
    this.route.navigate(["adminlogin"]);
}

getUsers(): void{
  this.UsServ.getAllUsers().subscribe(res=>this.retrieveUsers=res);
}

storeUser(UserRef:NgForm){
  
  this.UsServ.storeUser(UserRef.value).subscribe(res=>this.storeUs=res,error=>console.log(error),()=>this.getUsers());  
  UserRef.reset();
}
deleteUser(pid:number){
  
  this.UsServ.deleteUserInfo(pid).
  subscribe(res=>this.deleteUs=res,error=>console.log(error),()=>this.getUsers());
}

updateUser(user:UserEntity){
 
  this.flag=true;
  this.id=user.id;
  this.userfullname=user.userfullname;
  this.gender=user.gender;
  this.contact=user.contact;
  this.address=user.address;
  this.city = user.city;
  this.state=user.state;
}

updateUserDetails() {
  let user ={"id":this.id,"userfullname":this.userfullname,"contact":this.contact,"gender":this.gender,"address":this.address,"city":this.city,"state":this.state}
  this.UsServ.updateUserInfo(user).subscribe(result=>this.updateUs=result,
  error=>console.log(error),
  ()=>{
  this.getUsers();
  this.flag=false;  
  })
}

// CRUD on Books
getBooks(): void{
  this.bookServ.getAllBooks().subscribe(res=>this.getbooks=res);
}

storeBook(BookRef:NgForm){
  
  this.bookServ.storeBook(BookRef.value).subscribe(res=>this.storeMsg=res,error=>console.log(error),()=>this.getBooks());  
  BookRef.reset();
}
deleteBook(pid:number){
  
  this.bookServ.deleteBookInfo(pid).
  subscribe(res=>this.deleteMsg=res,error=>console.log(error),()=>this.getBooks());
}

updateBook(bk:BookEntity){
 
  this.fl=true;
  this.bid= bk.bid;
  this.bookname = bk.bookname;
  this.author = bk.author;
  this.price = bk.price;
  this.bookgenre=bk.bookgenre;
  this.bookimage = bk.bookimage;
}

updateBookDetails() {
  let book ={"bid":this.bid,"bookname":this.bookname,"author":this.author,"bookgenre":this.bookgenre,"bookimage":this.bookimage,"price":this.price}
  this.bookServ.updateBookInfo(book).subscribe(result=>this.updateMsg=result,
  error=>console.log(error),
  ()=>{
  this.getBooks();
  this.fl=false;  
  })
}




}

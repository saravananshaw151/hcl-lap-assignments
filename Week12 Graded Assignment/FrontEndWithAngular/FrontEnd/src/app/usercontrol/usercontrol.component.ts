import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BookEntity } from '../book-entity';
import { BookService } from '../book.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-usercontrol',
  templateUrl: './usercontrol.component.html',
  styleUrls: ['./usercontrol.component.css']
})
export class UsercontrolComponent implements OnInit {

  RegUser:string="";
  LogUser:string="";

  Books:Array<BookEntity>=[];

  UserLogRef = new FormGroup({
    useremail: new FormControl("",[Validators.required,Validators.pattern("\[a-z]+[0-9]+@gmail.com")]),
    password:new FormControl("",[Validators.required,Validators.minLength(6)])
  })
  
  UserRegRef = new FormGroup({
    userfullname: new FormControl("",[Validators.required,Validators.minLength(6)]),
    useremail:new FormControl("",[Validators.required,Validators.pattern("\[a-z]+[0-9]+@gmail.com")]),
     password:new FormControl("",[Validators.required,Validators.minLength(6)]),
    contact:new FormControl("",[Validators.required,Validators.minLength(10),Validators.maxLength(10)]),
    gender: new FormControl("",[Validators.required]),
    address:new FormControl("",[Validators.required]),
    city: new FormControl("",[Validators.required]),
    state:new FormControl("",[Validators.required])
  })

  constructor(public usSer:UserService,public route:Router,public bkser:BookService) { }

  ngOnInit(): void {
    this.getBooks();
  }

  
  userLogin():void{
    this.usSer.userSignIn(this.UserLogRef.value).subscribe(result=>{
      if(result.startsWith("Welcome")){
        sessionStorage.setItem("usname",this.UserLogRef.value.useremail);
        this.route.navigate(["userhome"]);
      }
      else{
        this.LogUser=result;
        this.UserLogRef.reset();} }
      ,err=>console.log(err),()=>console.log("User Login"));
   
  }

  userRegister():void{
   this.usSer.userSignUp(this.UserRegRef.value).subscribe(res=>this.RegUser=res,err=>console.log(err),()=>console.log("Register User"));
   this.UserRegRef.reset();
  }
 
  getBooks():void{
    this.bkser.getAllBooks().subscribe(res=>this.Books=res);
  }


}

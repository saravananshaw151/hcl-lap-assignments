export class BookEntity {
    
    constructor(
       public bid:number,
       public bookname:string,
       public author:string,
       public bookgenre:string,
       public bookimage:string,
       public price:number
    ){ 
    }
}
